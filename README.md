# MICROSERVICEAWS

## Requirements
- Docker
- Make

## Structure
![Scheme](estrutura.png)
 
## Running
### Linux

+ Build:
``` 
make build 
```

+ Run:
``` 
make up
```
