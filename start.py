# !/usr/bin/env python

import os, json, sys, argparse

sys.path.insert(0, "./src/")

from Event import Event
from ConfigFile import ConfigFile

parser = argparse.ArgumentParser()
parser.add_argument("--build", help="Build chalice image", default=False)
parser.add_argument("--event", help="Inform a service to throw event", default=False)
parser.add_argument("--up", help="Inform a service to be up", default=False)
parser.add_argument("--port", help="Inform a port to service start ( Default: 8000 )", default=8000)
parser.add_argument("--net", help="Inform a network name to connect the service", default=False)
parser.add_argument("--lib", help="Install the requirements first to use services", default=False)
parser.add_argument("--create_network", help="Write the network name to use on parameter --net", default=False)
parser.add_argument("--stack", help="To up all services by App name", default=False)
args = parser.parse_args()

if not args.up and not args.event and not args.build and not args.stack:
    if not args.create_network:
        raise Exception("Nothing service found on parameters (--up or --event)")

    os.system("docker network create " + args.create_network + " || true ")


def stack(stack_name, port):
    os.system("echo '\n >>>>>>>> Start stack: " + stack_name + " <<<<<<<<<'")
    os.system("docker stop $(docker ps -aq --filter 'status=running')")
    os.system("docker network rm " + stack_name)
    print("docker network create " + stack_name)
    os.system("docker network create " + stack_name + " || true ")

    for record in os.listdir("./services"):
        if record == '.gitkeep':
            continue

        config_file_object = ConfigFile(record)
        if config_file_object.get_project_name().lower() == stack_name.lower():
            service(config_file_object, port)
            port += 1


def service(config_file_object, start_port):
    os.system("echo '\n =================== " + config_file_object.get_service_name() + " =================='")
    stack_name = config_file_object.get_project_name() if args.net is False else str(args.net)

    if config_file_object.get_event_type() in ["api", "queue", "kinesis"]:
        os.system("rm -rf ./logs/" + config_file_object.get_service_name() + ".log")
        os.system('docker run --rm -it ' \
                  '--name ' + config_file_object.get_service_name() + ' ' \
                                                                      '-e LOCAL_USER_ID=`id -u $USER` ' \
                                                                      '--memory="' + str(
            config_file_object.get_memory()) + 'm" ' \
                                               '--volume "$(pwd)/services/' + config_file_object.get_service_name() + '":"/app" ' \
                                                                                                                      '--volume "$HOME/.aws":"/root/.aws" ' \
                                                                                                                      '--net "' + stack_name + '" ' \
                                                                                                                                               '--publish ' + str(
            start_port) + ':8000 ' \
                          '--workdir "/app" -d aws-chalice chalice local --host 0.0.0.0 ')
        os.system(
            "docker logs -f " + config_file_object.get_service_name() + " > ./logs/" + config_file_object.get_service_name() + ".log &")
        os.system("docker inspect " + config_file_object.get_service_name() + " --format '" \
                                                                              "External Access: http://{{range .NetworkSettings.Networks}}{{.IPAddress}}:" + str(
            start_port) + "{{end}} \n" \
                          "Internal Docker network http access: http://" + config_file_object.get_service_name() + ":8000'")


def lib(service):
    os.system('docker exec ' + service + ' pip install -r requirements.txt -t vendor')

    internal_file = "./services/" + service + "/requirements_internal.txt"
    if os.path.exists(internal_file):
        lines = [line.rstrip('\n') for line in open(internal_file)]
        for value in lines:
            os.system(
                'git clone git@bitbucket.org:madeiramadeira/' + value + '.git ./services/' + service + '/chalicelib/src/' + value
            )


# def deploy(service):
#     os.system("cp ./deploy/Apigateway.py services/" + service + "/Apigateway.py")
#     os.system("cp ./deploy/Deployer.py services/" + service + "/Deployer.py")
#     os.system("cp ./deploy/Lambda.py services/" + service + "/Lambda.py")
#     os.system("docker exec -u user " + service + " chalice package .")
#     os.system("docker exec -u user " + service + " python Deployer.py --stage=dev --account_number=683720833731")


if args.stack:
    stack(args.stack, args.port)

if args.lib:
    lib(args.lib)

if args.up:
    config_file_object = ConfigFile(args.up)
    service(config_file_object, args.port)

if args.event:
    config_file_object = ConfigFile(args.event)
    eventObject = Event(config_file_object).to_string()
    os.system(
        'docker exec ' + args.event + ' python -c \'import sys, os; ' + config_file_object.env_vars_to_array() + ' sys.path.append("vendor/");from app import index; index(' + eventObject + ', "")\'')

# if args.deploy:
#     deploy(args.deploy)

if sys.argv[1] == "kill":
    os.system('docker kill $(docker ps -q)')

if args.build:
    os.system('docker rmi aws-chalice')
    os.system('docker build -t aws-chalice -f Dockerfile .')

