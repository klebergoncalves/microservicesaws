from chalice import Chalice
from chalice import BadRequestError
from chalicelib.sendmessages import SendMessages
from os import environ as env
import sys
import json

app = Chalice(app_name='customers')

@app.route('/' + app.app_name + '/', methods=['POST'])
def index():
    try:
        payload = validate_payload()
        response = send_to_sns(
            payload,
            'arn:aws:sns:' + env.get('REGION_NAME') + ':' + env.get('ACCOUNT_ID') + ':api-sns-customers'
        )
    except Exception as e:
        raise BadRequestError('Error <api_customers>: ' + str(e))

    return response

def validate_payload(layout):

    if not app.current_request.json_body:
        raise Exception('Payload not found!')

    payload = app.current_request.json_body
#     TODO: validar payload
    print(payload)

    return payload

def send_to_sns(payload, topic):
    try:

        sendMessages = SendMessages()
        sendMessages.send_to_topic(topic, payload)

        response = {"error": False, "message": "Sended for SNS"}

    except Exception as e:
        raise e

    return response

