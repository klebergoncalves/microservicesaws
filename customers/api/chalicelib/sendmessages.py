import boto3
import json
import os


class SendMessages:

    @staticmethod
    def send_to_topic(topic, payload):
        client = SendMessages.__aws_client__('sns')

        content = json.dumps(
            {
                "payload": payload
            }
        )

        response = client.publish(
            TopicArn=topic,
            Subject='SNS_SEND_ARN',
            Message=content
        )
        print('send_to_topic')
        print(response)

    @staticmethod
    def __aws_client__(service):
        client = boto3.client(
            service,
            aws_access_key_id=os.getenv('KEY'),
            aws_secret_access_key=os.getenv('SECRET'),
            region_name=os.getenv('REGION_NAME')
        )
        return client