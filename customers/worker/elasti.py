from datetime import datetime
import os
import requests
import json
from requests_aws4auth import AWS4Auth


class Logger:

    @staticmethod
    def log(content, error=''):
        try:
            now = datetime.now()
            data = {
                "message": "BMS Receive",
                "context": {
                    "command": "bms-register",
                    "method": "bms-receive-wms",
                    "payload": content
                },
                "level": 200,
                "level_name": "INFO",
                "channel": "lambda",
                "datetime": now.isoformat()
            }

            if error:
                data["level"] = 500
                data["level_name"] = "ERROR"
                data["context"]["error"] = error

            # auth = AWS4Auth(
            #     os.getenv('AWS_KEY'),
            #     os.getenv('AWS_SECRET'),
            #     os.getenv('AWS_REGION_NAME'),
            #     'es'
            # )

            headers = {
                'content-type': 'application/json'
            }

            print(os.getenv('ELASTIC'))
            response = requests.post(
                os.getenv('ELASTIC'),
                data=json.dumps(data),
                headers=headers
            )
            print(response.text)

            result = json.loads(response.text)
            if "errors" in result:
                raise Exception('Error: ' + str(result))

            return True
