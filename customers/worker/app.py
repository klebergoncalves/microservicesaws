from chalice import Chalice
from chalice import BadRequestError
from os import environ as env
from datetime import date
import pymysql
import json
import requests

app = Chalice(app_name='customers_worker')

@app.on_sqs_message(queue='customers-save-db', batch_size=1)
def index():
    try:
        connection = get_connection()

        payload = app.current_request.json_body

        customer_arrears = str(payload.get('customer_arrears'))

        with connection.cursor() as cursor:

            print('INSERT CUSTOMER ')
            sql_customer = ('INSERT INTO customers ( ' +
                    'document, ' +
                    'name, ' +
                    'address, ' +d ap
                    'age, ' +
                    'date_check_document ' +
                    ') VALUES ( ' +
                    '\'' + str(payload.get('document')) + '\', ' +
                    '\'' + str(payload.get('name')) + '\', ' +
                    '\'' + str(payload.get('address')) + '\', ' +
                    '\'' + str(payload.get('date_check_document'))+ '\' ' +
                    ');')
            cursor.execute(sql_customer)

            customer_id = get_customer(str(payload.get('document')), cursor)

            if customer_id:
                if  customer_arrears:
                    print('INSERT CUSTOMER ARREARS')
                    sql_customer_arrears = ('INSERT INTO customer_arrears ( ' +
                             'customer_id, ' +
                             'company_name, ' +
                             'company_document, ' +
                             'amount ' +
                             ') VALUES ( ' +
                             '\'' + str(customer_id) + '\', ' +
                             '\'' + str(payload.get('company_name')) + '\', ' +
                             '\'' + str(payload.get('company_document')) + '\', ' +
                             '\'' + str(payload.get('amount'))+ '\' ' +
                             ');')
                    cursor.execute(sql_customer_arrears)

        connection.commit()

    except Exception as e:
        print('Error <api_customers>: ' + str(e))
        raise BadRequestError('Error <api_customers>: ' + str(e))
    finally:
        connection.close()

    return {"error": False, "message": "Saved customer into DB"}

@app.on_sqs_message(queue='customers-save-elk', batch_size=1)
def index():
    try:
        payload = app.current_request.json_body

        headers = {
            'content-type': 'application/json'
        }

        response = requests.post(
            env.get('ELASTIC'),
            data=payload,
            headers=headers
        )
        print(response.text)

    except Exception as e:
        print('Error <api_customers>: ' + str(e))
        raise BadRequestError('Error <api_customers>: ' + str(e))

    return {"error": False, "message": "Saved customer into ELK"}


def get_connection():

    connection = pymysql.connect(
        host=env.get('DATABASE_HOST'),
        user=env.get('DATABASE_USER'),
        password=env.get('DATABASE_PASS'),
        db=env.get('DATABASE_NAME'),
        cursorclass=pymysql.cursors.DictCursor
    )
    return connection


def get_customer(document, cursor):
    sql = '''
                select id
                from customers
                where document = '{0}'
            '''.format(document)

    cursor.execute(sql)
    result = cursor.fetchall()

    if len(result) > 0:
        return result[0]

    return False