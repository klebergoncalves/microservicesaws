<?php

namespace AppBundle\Service;

use JMS\Serializer\SerializerBuilder;
use Symfony\Bridge\Monolog\Logger;

class CustomersService
{
    /**
     * @var Logger
     */
    private $logger;

    /**
     * CustomersService constructor.
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

}