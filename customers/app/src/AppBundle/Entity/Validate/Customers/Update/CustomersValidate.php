<?php

namespace AppBundle\Entity\Validate\Customers\Update;

use AppBundle\Entity\Validate\Users\CustomersBaseValidate;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;

class CustomersValidate extends CustomersBaseValidate
{
    /**
     * @Assert\NotBlank(message = "Id is required")
     * @Assert\Type(
     *     type="string",
     *     message="Id - The value {{ value }} is not a valid {{ type }}."
     * )
     * @Type("string")
     */
    public $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}