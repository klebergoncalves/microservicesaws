<?php

namespace AppBundle\Entity\Validate\Customers\Create;

use AppBundle\Entity\Validate\Users\CustomersBaseValidate;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;

class CustomersValidate extends CustomersBaseValidate
{
    /**
     * @Assert\NotBlank(message = "Name is required")
     * @Assert\Type(
     *     type="string",
     *     message="Name - The value {{ value }} is not a valid {{ type }}."
     * )
     * @Type("string")
     */
    public $name;

    /**
     * @Assert\NotBlank(message = "Email is required")
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     * @Type("string")
     */
    public $email;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
}