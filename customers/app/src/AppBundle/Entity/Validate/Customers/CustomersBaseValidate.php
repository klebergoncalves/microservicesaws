<?php

namespace AppBundle\Entity\Validate\Customers;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;

class CustomersBaseValidate
{
    /**
     * @Assert\Type(
     *     type="string",
     *     message="Id - The value {{ value }} is not a valid {{ type }}."
     * )
     * @Type("string")
     */
    public $id;

    /**
     * @Assert\Type(
     *     type="string",
     *     message="name - The value {{ value }} is not a valid {{ type }}."
     * )
     * @Type("string")
     */
    public $name;

    /**
     * @Assert\Type(
     *     type="string",
     *     message="document - The value {{ value }} is not a valid {{ type }}."
     * )
     * @Type("string")
     */
    public $document;

    /**
     * @Assert\Type(
     *     type="string",
     *     message="age - The value {{ value }} is not a valid {{ type }}."
     * )
     * @Type("string")
     */
    public $age;

    /**
     * @Assert\Type(
     *     type="string",
     *     message="address - The value {{ value }} is not a valid {{ type }}."
     * )
     * @Type("string")
     */
    public $address;

    /**
     * @Assert\Type(
     *     type="string",
     *     message="dateCheckDocument - The value {{ value }} is not a valid {{ type }}."
     * )
     * @Type("string")
     */
    public $dateCheckDocument;

    /**
     * @Assert\DateTime
     * @Type("DateTime<'d-m-Y'>")
     */
    public $createdAt;

    /**
     * @Assert\Type("integer")
     * @Type("integer")
     */
    public $createdBy;

    /**
     * @Assert\DateTime
     * @Type("DateTime<'d-m-Y'>")
     */
    public $changedAt;

    /**
     * @Assert\Type("integer")
     * @Type("integer")
     */
    public $changedBy;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param mixed $document
     */
    public function setDocument($document)
    {
        $this->document = $document;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getDateCheckDocument()
    {
        return $this->dateCheckDocument;
    }

    /**
     * @param mixed $dateCheckDocument
     */
    public function setDateCheckDocument($dateCheckDocument)
    {
        $this->dateCheckDocument = $dateCheckDocument;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return mixed
     */
    public function getChangedAt()
    {
        return $this->changedAt;
    }

    /**
     * @param mixed $changedAt
     */
    public function setChangedAt($changedAt)
    {
        $this->changedAt = $changedAt;
    }

    /**
     * @return mixed
     */
    public function getChangedBy()
    {
        return $this->changedBy;
    }

    /**
     * @param mixed $changedBy
     */
    public function setChangedBy($changedBy)
    {
        $this->changedBy = $changedBy;
    }

}