<?php

namespace AppBundle\Entity\Database;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ORM\Table(name="customers")
 * @ORM\HasLifecycleCallbacks
 */
class Customers
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Type("string")
     * @
     */
    public $id;

    /**
     * @ORM\Column(name="name", type="string")
     * @Type("string")
     */
    public $name;

    /**
     * @ORM\Column(name="document", type="string")
     * @Type("string")
     */
    public $document;

    /**
     * @ORM\Column(name="age", type="string")
     * @Type("string")
     */
    public $age;

    /**
     * @ORM\Column(name="address", type="string")
     * @Type("string")
     */
    public $address;

    /**
     * @ORM\Column(name="date_check_document", type="string")
     * @Type("string")
     */
    public $dateCheckDocument;

    /**
     * @ORM\Column(name="created_at", type="DateTime<'d-m-Y'>")
     * @Type("DateTime<'U'>")
     */
    public $createdAt;

    /**
     * @ORM\Column(name="created_by", type="string")
     * @Type("string")
     */
    public $createdBy;

    /**
     * @ORM\Column(name="changed_at", type="DateTime<'d-m-Y'>")
     * @Type("DateTime<'U'>")
     */
    public $changedAt;

    /**
     * @ORM\Column(name="changed_by", type="string")
     * @Type("string")
     */
    public $changedBy;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param mixed $document
     */
    public function setDocument($document)
    {
        $this->document = $document;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getDateCheckDocument()
    {
        return $this->dateCheckDocument;
    }

    /**
     * @param mixed $dateCheckDocument
     */
    public function setDateCheckDocument($dateCheckDocument)
    {
        $this->dateCheckDocument = $dateCheckDocument;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return mixed
     */
    public function getChangedAt()
    {
        return $this->changedAt;
    }

    /**
     * @param mixed $changedAt
     */
    public function setChangedAt($changedAt)
    {
        $this->changedAt = $changedAt;
    }

    /**
     * @return mixed
     */
    public function getChangedBy()
    {
        return $this->changedBy;
    }

    /**
     * @param mixed $changedBy
     */
    public function setChangedBy($changedBy)
    {
        $this->changedBy = $changedBy;
    }
}